package com.example.student1.interfacelesson;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    int k = 0;
    Button bClicker;
    TextView countClicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countClicker = (TextView) findViewById(R.id.count_clicker);

        bClicker = (Button) findViewById(R.id.b_clicker);
        bClicker.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.d("MainActivity","OnClick"); // Тест Логов
        k++; // считает сколько раз ты нажал
        countClicker.setText("" + k); // Выводит
    }
}
